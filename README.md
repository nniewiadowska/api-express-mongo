# REST API Express.js + MongoDB
Projekt **Zadania 1** zawiera aplikację magazynu produktów. Jest stworzony w Expressie i połączony z bazą danych MongoDB za pomocą pakietu `mongodb`. Aplikacja jest **asynchroniczna** i ma deploy (jeszcze nie do końca działający) na serwerze hostingowym **Netlify** pod adresem [university-task-api-in-express.netlify.app](university-task-api-in-express.netlify.app), a także zawiera następujące żądania:

1. **`GET`** na `/products` - zwraca wszystkie produkty; można filtrować i sortować według różnych kryteriów. Przykład requesta w Postmanie:
```
http://localhost:5000/products?filter={"name":{"$in":["Milk","Bread"]}}&sortBy=asc&sortField=price
# zwraca produkty o nazwie "Milk" lub "Bread" i sortuje rosnąco po cenie
```

2. **`POST`** na `/products` - dodaje nowy produkt uprzednio sprawdzając, czy nazwa jest unikalna. Przykład:

```sh
http://localhost:5000/products?name=Banana&price=50&description=monke&quantity=20&unit=pcs
```

3. **`PUT`** na `/products/:id` - edytuje istniejący produkt o podanym ID. Przykład:

```
http://localhost:5000/products/654ea5119946491cdb27d9aa
# przykładowa zawartość Body -> raw:
{
  "price": 15,
  "unit": "kg"
}
```

4. **`DELETE`** na `/products/:id` - usuwa istniejący produkt. Przykład:

```
http://localhost:5000/products/654ea5119946491cdb27d9aa
```

5. **`GET`** na `/products/report` - raportowanie stanu magazynu. Generuje raport o łącznej ilości produktów, łącznej cenie i wartości, a także osobne statystyki dla każdego produktu. używa do tego agregacji.

```
http://localhost:5000/products/report
```

6. dodatkowo: **`GET`** na `/products/:id` - zwraca pojedynczy produkt po ID. 
