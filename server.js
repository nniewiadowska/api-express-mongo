const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config({ path: "./config.env" });
const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
  console.log('Incoming Request:', req.method, req.url);
  next();
});
app.use(require("./routes/record"));
const dbo = require("./db/conn");

dbo.connectToServer(async function (err) {
  if (err) {
    process.exit(1);
  } else {
    const db = dbo.getDb();
    const collectionCount = await db.collection("products").countDocuments();

    // przy łączeniu się z serwerem dodaję produkty do bazy tylko wtedy, kiedy jest ona pusta
    if (collectionCount === 0) {
      const productsData = [
        { name: "Milk", price: 5, description: "Milk from a cow", quantity: 100, unit: "l" },
        { name: "Bread", price: 6, description: "The breadest of bread", quantity: 15, unit: "pcs" },
        { name: "Egg", price: 2, description: "That's so eggy", quantity: 50, unit: "pcs" },
        { name: "Lettuce", price: 4, description: "Thumbelina's home", quantity: 25, unit: "kg" },
        { name: "Apple", price: 3, description: "Eve made a mistake once", quantity: 60, unit: "kg" }
      ];

      db.collection("products").insertMany(productsData, function (err, res) {
        if (err) {
          console.error("Error inserting documents:", err);
          process.exit(1);
        } else {
          console.log("Documents inserted:", res.insertedCount);
        }
      });
    }

    app.listen(port, () => {
      console.log(`Server is running on port: ${port}`);
    });

    process.on('SIGINT', () => {
      dbo.closeConnection();
      process.exit(0);
    });

  }
});
